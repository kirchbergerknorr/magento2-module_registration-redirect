<?php
/**
 * @author      Roland Schilffarth <rs@kirchbergerknorr.de>
 * @copyright   Copyright (c) 2017 kirchbergerknorr GmbH (www.kirchbergerknorr.de)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Kirchbergerknorr\RegistrationRedirect\Plugin\Controller\Account;

use Magento\Customer\Controller\Account\CreatePost;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Model\Session;

class CreatePostPlugin
{

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var Session
     */
    private $customerSession;


    /**
     * @param RequestInterface $request
     * @param Session $customerSession
     */
    public function __construct(RequestInterface $request, Session $customerSession)
    {
        $this->request = $request;
        $this->customerSession = $customerSession;
    }

    /**
     * Redirect customers to checkout after registration, if they registered during the checkout. (independent on cart)
     *
     * @param CreatePost $subject
     * @param Redirect $result
     * @return Redirect
     */
    public function afterExecute(CreatePost $subject, Redirect $result)
    {
        $redirectRoute = $this->getRedirectRoute();
        if ($redirectRoute && $this->customerSession->isLoggedIn()) {
            $result->setPath($redirectRoute);
            $this->unsetRedirectRoute();
        }
        return $result;
    }

    /**
     * Set customer session redirect_route data for proper redirecting after successful registration.
     *
     * @return null|string
     */
    private function getRedirectRoute()
    {
        if (!$this->customerSession->getRedirectRoute()) {
            $redirectRoute = $this->request->getParam('redirect');
            if ($redirectRoute) {
                $this->customerSession->setRedirectRoute($redirectRoute);
            }
        }

        return $this->customerSession->getRedirectRoute();
    }

    /**
     * @return void
     */
    private function unsetRedirectRoute()
    {
        $this->customerSession->setRedirectRoute(null);
    }

}
