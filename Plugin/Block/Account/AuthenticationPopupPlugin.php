<?php
/**
 * @author      Roland Schilffarth <rs@kirchbergerknorr.de>
 * @copyright   Copyright (c) 2017 kirchbergerknorr GmbH (www.kirchbergerknorr.de)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Kirchbergerknorr\RegistrationRedirect\Plugin\Block\Account;

use Magento\Customer\Block\Account\AuthenticationPopup;

class AuthenticationPopupPlugin
{

    /**
     * Sets redirect URL after customer registered successful.
     *
     * @param AuthenticationPopup $subject
     * @return string
     */
    public function afterGetCustomerRegisterUrlUrl(AuthenticationPopup $subject)
    {
        return $subject->getUrl('customer/account/create', ['redirect' => 'checkout']);
    }

}
