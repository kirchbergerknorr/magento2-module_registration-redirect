<?php
/**
 * @author      Roland Schilffarth <rs@kirchbergerknorr.de>
 * @copyright   Copyright (c) 2017 kirchbergerknorr GmbH (www.kirchbergerknorr.de)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Kirchbergerknorr\RegistrationRedirect\Plugin\Model;

use Magento\Customer\Model\Url;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\UrlInterface;

class UrlPlugin
{

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;


    /**
     * @param RequestInterface $request
     * @param UrlInterface $urlBuilder
     */
    public function __construct(RequestInterface $request, UrlInterface $urlBuilder)
    {
        $this->request = $request;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Sets redirect URL after customer registered successful.
     *
     * @param Url $subject
     * @return string
     */
    public function afterGetRegisterPostUrl(Url $subject)
    {
        $redirectParam = $this->request->getParam('redirect');
        $urlRedirect = $redirectParam ? ['redirect' => $redirectParam] : null;
        return $this->urlBuilder->getUrl('customer/account/createpost', $urlRedirect);
    }

}
