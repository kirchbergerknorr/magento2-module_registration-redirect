# Kirchbergerknorr RegistrationRedirect

**Extension for Magento 2**

--- 

## Overview

Redirecting customer to checkout after successful registration, if user registered during purchase.

## Requirements and setup

Tested for Magento 2.1.10

This extension can be installed using [Composer](https://getcomposer.org/doc/01-basic-usage.md).

## Details

There can be added different redirects after the customer registered successful.

For adding another redirect, simply pass a query parameter `redirect` containing your desired endpoint to be loaded after registration was successful to the customer/account/create route.

## Authors

Roland Schilffarth [rs@kirchbergerknorr.de](mailto:rs@kirchbergerknorr.de)